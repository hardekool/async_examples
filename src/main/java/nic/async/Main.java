package nic.async;

import java.util.concurrent.TimeUnit;

public class Main {

    private static boolean stopThread;
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Starting work");
        new Thread(() -> {
            int i = 0;
            while (!stopThread) {
                i++;
            }
        }).start();
        TimeUnit.SECONDS.sleep(2);
        System.out.println("Attempting to stop");
        stopThread = true;
    }
}
