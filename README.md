Asynchronous examples with Java
* Threads with Runnable
* Executors and Callables
* Memory consistency
* Barriers
* Locking